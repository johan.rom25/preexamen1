/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preexamen1;

/**
 *
 * @author ionix
 */
public class Perecederos extends Producto{
   private String temp;
   private String fechaCad;

    public Perecederos() {
        this.temp = "";
        this.fechaCad = "";
    }

    public Perecederos(String temp, String fechaCad) {
        this.temp = temp;
        this.fechaCad = fechaCad;
    }

    public Perecederos(String temp, String fechaCad, int idProducto, String nomProducto, int uniProducto, float precioUnitario) {
        super(idProducto, nomProducto, uniProducto, precioUnitario);
        this.temp = temp;
        this.fechaCad = fechaCad;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getFechaCad() {
        return fechaCad;
    }

    public void setFechaCad(String fechaCad) {
        this.fechaCad = fechaCad;
    }
   
    @Override
    public float calcularPrecio(){
        float precioPerecedero=0.0f;
        switch (this.getUniProducto()) {
            case 1:
                precioPerecedero = this.precioUnitario * 1.03f;
                precioPerecedero = precioPerecedero * 1.5f;
                break;
            case 2:
                precioPerecedero = this.precioUnitario * 1.05f;
                precioPerecedero = precioPerecedero * 1.5f;
                break;
            case 3:
                precioPerecedero = this.precioUnitario * 1.04f;
                precioPerecedero = precioPerecedero * 1.5f;
                break;
        }
        return precioPerecedero;
    }
}
