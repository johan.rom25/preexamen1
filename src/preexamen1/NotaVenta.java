/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preexamen1;

/**
 *
 * @author ionix
 */
public abstract class NotaVenta{
    protected int numNota;
    protected String fecha;
    protected Perecederos perecedero;
    protected String concepto;
    protected int cantidad;
    protected int tipoPago;
    
    public NotaVenta() {
        this.numNota = 0;
        this.fecha = "";
        this.perecedero = new Perecederos();
        this.concepto = "";
        this.cantidad = 0;
        this.tipoPago = 0;
    }

    public NotaVenta(int numNota, String fecha, Perecederos perecedero, String concepto, int cantidad, int tipoPago) {
        this.numNota = numNota;
        this.fecha = fecha;
        this.perecedero = perecedero;
        this.concepto = concepto;
        this.cantidad = cantidad;
        this.tipoPago = tipoPago;
    }

    public int getNumNota() {
        return numNota;
    }

    public void setNumNota(int numNota) {
        this.numNota = numNota;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Perecederos getPerecedero() {
        return perecedero;
    }

    public void setPerecedero(Perecederos perecedero) {
        this.perecedero = perecedero;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(int tipoPago) {
        this.tipoPago = tipoPago;
    }
    
    public abstract float calcularPago();

}
