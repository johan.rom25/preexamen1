/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preexamen1;

/**
 *
 * @author ionix
 */
public class Factura extends NotaVenta implements Impuesto{
    private String rfc;
    private String nomCliente;
    private String domicilio;
    private String fechaFactura;

    public Factura() {
        this.rfc = "";
        this.nomCliente = "";
        this.domicilio = "";
        this.fechaFactura = "";
    }

    public Factura(String rfc, String nomCliente, String domicilio, String fechaFactura, int numNota, String fecha, Perecederos perecedero, String concepto, int cantidad, int tipoPago) {
        super(numNota, fecha, perecedero, concepto, cantidad, tipoPago);
        this.rfc = rfc;
        this.nomCliente = nomCliente;
        this.domicilio = domicilio;
        this.fechaFactura = fechaFactura;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getNomCliente() {
        return nomCliente;
    }

    public void setNomCliente(String nomCliente) {
        this.nomCliente = nomCliente;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getFechaFactura() {
        return fechaFactura;
    }

    public void setFechaFactura(String fechaFactura) {
        this.fechaFactura = fechaFactura;
    }
    
    @Override
    public float calcularImpuesto() {
        return this.calcularPago() * .16f;
    }
    
    @Override
    public float calcularPago() {
        return this.perecedero.calcularPrecio() * this.cantidad;
    }
}
