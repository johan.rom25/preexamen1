/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preexamen1;

/**
 *
 * @author ionix
 */
public abstract class Producto {
    protected int idProducto;
    protected String nomProducto;
    protected int uniProducto;
    protected float precioUnitario;

    public Producto() {
        this.idProducto = 0;
        this.nomProducto = "";
        this.uniProducto = 0;
        this.precioUnitario = 0.0f;
    }

    
    
    public Producto(int idProducto, String nomProducto, int uniProducto, float precioUnitario) {
        this.idProducto = idProducto;
        this.nomProducto = nomProducto;
        this.uniProducto = uniProducto;
        this.precioUnitario = precioUnitario;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public String getNomProducto() {
        return nomProducto;
    }

    public void setNomProducto(String nomProducto) {
        this.nomProducto = nomProducto;
    }

    public int getUniProducto() {
        return uniProducto;
    }

    public void setUniProducto(int uniProducto) {
        this.uniProducto = uniProducto;
    }

    public float getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(float precioUnitario) {
        this.precioUnitario = precioUnitario;
    }
    
    public abstract float calcularPrecio();
    
}
