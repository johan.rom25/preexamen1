/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preexamen1;

/**
 *
 * @author ionix
 */
public class NoPerecederos extends Producto{
    public String lote;

    public NoPerecederos() {
        this.lote = "";
    }

    public NoPerecederos(String lote) {
        this.lote = lote;
    }

    public NoPerecederos(String lote, int idProducto, String nomProducto, int uniProducto, float precioUnitario) {
        super(idProducto, nomProducto, uniProducto, precioUnitario);
        this.lote = lote;
    }

    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }
    
   @Override
    public float calcularPrecio(){
        return this.precioUnitario*1.5f;
    }

    
}

   
    
    

    

    
    
    
    

